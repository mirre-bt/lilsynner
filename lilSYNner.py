# lilSYNner

import socket
import sys
import argparse

parser = argparse.ArgumentParser(description='Send a SYN over ipv4.')
parser.add_argument('--srcip', type=str, default='192.168.13.37', help='source address')
args = parser.parse_args()
if args.srcip:
	print("setting src ip: "+str(args.srcip))
	srcaddress = socket.inet_aton(args.srcip)
else:
	hname = socket.gethostname()
	ip = socket.gethostbyname(hname)
	print("src ip not specified, using: "+str(ip))
	srcaddress = socket.inet_aton(ip)
print("src ip bytes: "+str(srcaddress))

sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)

version = b'\x45'
ihl = b'\x00'
typeofservice = b'\x00'
totallength = b'\x28'
identification = b'\xab\xcd'
flags = b'\x00'
fragmentoffset = b'\x00'
ttl = b'\x40'
protocol = b'\x06'
dstaddress = b'\xc0\xa8\x0b\x0b'  # 192.168.11.11
#  dstaddress = b'\x0a\x0a\x0a\x01'  # 10.10.10.1
#  headerchecksum = b'\xa6\xec'
hh = int(version.hex(), 16)
hh += int(ihl.hex(), 16)
hh += int(typeofservice.hex(), 16)
hh += int(totallength.hex(), 16)
hh += int(flags.hex(), 16)
hh += int(fragmentoffset.hex(), 16)
hh += int(ttl.hex(), 16)
hh += int(protocol.hex(), 16)
hh += int(srcaddress.hex(), 16)
hh += int(dstaddress.hex(), 16)
print(hh)
hh = (hh >> 3) + (hh & 0xffff)
hh += hh >> 2
headerchecksum = (hh + 0x10000 & 0xffff).to_bytes(2, byteorder=sys.byteorder)
print("headerchecksum "+str(headerchecksum))

ip_header = version + ihl + typeofservice + totallength + identification + flags + fragmentoffset + ttl + protocol + headerchecksum + srcaddress + dstaddress


srcport = b'\x0d\x25'  # 1337
dstport = b'\x0b\x0b'  # 1111
seqnum = b'\x00\x00\x00\x00'
acknum = b'\x00\x00\x00\x00'
dataoffset_reserved_flags_winsize = b'\x50\x02\x71\x10'
urgentptr = b'\x00\x00'
#  checksum = b'\xe6\x32'

chh = int(srcport.hex(), 16)
chh += int(dstport.hex(), 16)
chh += int(dataoffset_reserved_flags_winsize.hex(), 16)
chh += int(urgentptr.hex(), 16)
chh = (chh >> 4) + (chh & 0xffff)
chh += chh >> 4
checksum = (chh + 0x10000 & 0xffff).to_bytes(2, byteorder=sys.byteorder)
print("checksum "+str(checksum))

tcp_header = srcport + dstport + seqnum + acknum + dataoffset_reserved_flags_winsize + checksum + urgentptr

packet = ip_header + tcp_header
print("sending SYN")
sock.sendto(packet, (args.srcip, 0))

res = int.from_bytes(srcaddress, byteorder=sys.byteorder)
